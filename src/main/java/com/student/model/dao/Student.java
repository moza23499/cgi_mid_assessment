package com.student.model.dao;

public class Student {
	private int id;
	private String fname;
	private String subjectname;
	private int subjectmarks;
	private int testnumber;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getSubjectname() {
		return subjectname;
	}
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}
	public int getSubjectmarks() {
		return subjectmarks;
	}
	public void setSubjectmarks(int subjectmarks) {
		this.subjectmarks = subjectmarks;
	}
	public int getTestnumber() {
		return testnumber;
	}
	public void setTestnumber(int testnumber) {
		this.testnumber = testnumber;
	}
	public Student(int id, String fname, String subjectname, int subjectmarks, int testnumber) {
		super();
		this.id = id;
		this.fname = fname;
		this.subjectname = subjectname;
		this.subjectmarks = subjectmarks;
		this.testnumber = testnumber;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", fname=" + fname + ", subjectname=" + subjectname + ", subjectmarks="
				+ subjectmarks + ", testnumber=" + testnumber + "]";
	}
	
	
	
	
}